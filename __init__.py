aalt = "ACDEFGHIKLMNOPQRSTUVWY" ###amino acid letter table
class Protein:
		def __init__(self,id,name,os,ox,gn,pe,sv,seq):
			self.id = id
			self.name = name
			self.os = os
			self.ox = ox
			self.gn = gn
			self.pe = pe
			self.sv = sv
			self.seq = seq
			self.size = len(seq)
			aac = {}
			for char in aalt:
				aac[char]=seq.count(char)
			self.aminoacidcounts = aac
			self.values = [self.id,self.name,self.os,self.ox,self.gn,self.pe,self.sv,self.seq,self.size,self.aminoacidcounts]
		def __getitem__(self,Index):
			return self.values[Index]
#rich comparison things, only compares size, unnecessary spagoot deleted
		def __eq__(self, other):
		        return ((self.size) == (other.size))
		def __lt__(self, other):
		        return ((self.size) < (other.size))
		def __gt__(self, other):
		        return ((self.size) > (other.size))
		def __ne__(self, other):
		        return ((self.size) != (other.size))
		def __le__(self, other):
		        return ((self.size) <= (other.size))
		def __ge__(self, other):
		        return ((self.size) >= (other.size))
#repr stuff, returns 
		def __repr__(self):
			return '%s id: %s'%(self.name,self.id)

def load_fasta(filename):
	txt_read = open(filename, "r")
	fst = txt_read.read()
	txt_read.close()
	fstsplit = fst.split("\n>")
#split fasta file where a newline follows a >. otherwise it creates a first elemetn before the initial > which will be empty and cause an off-by-one error
	pnum = len(fstsplit)
	iter = 0
	protlist = []
	protlen=0
#iterate through the faste file segments, index where the distinct separators are for the different values then use these to extract the variables themselves,
#format them accordingly, load then to a Protein class and append it to a list which the function returns.
	for i in fstsplit:
		spf = i
		first = spf.find("|")
		second = spf.find("|", first+1)
		third = spf.index("OS=", second)
#I'm 100% sure there is a better, more straightforward solution to this...
		fourth = spf.index("OX=", third)
		fifth = spf.index("GN=", fourth)
		sixth = spf.index("PE=", fifth)
		seventh = spf.index("SV=", sixth)
		eighth = spf.find("\n", seventh)
		idstr = spf[first+1:second]
		namestr = spf[second+1:third]
		osstr = spf[third+3:fourth]
		oxstr = spf[fourth+3:fifth]
		gnstr = spf[fifth+3:sixth]
		pestr = spf[sixth+3:seventh]
		svstr = spf[seventh+3:eighth]
		seqstr = spf[eighth:]
		protlist.append( Protein(idstr.strip(), namestr.strip(), osstr.strip(), int(oxstr.strip()), gnstr.strip(), int(pestr.strip()), int(svstr.strip()), seqstr.replace('\n','')))
		iter = iter+1
	return(protlist)
#sort proteins by PE value in decreasing order. PE attribute is the 5th one in the list (0-indexing)
def sort_proteins_pe(proteins):
	protpe = sorted(proteins, key = lambda x: x[5], reverse = True)
	return protpe
#sort proteins by frequency of an aminoacid letter: the  countra is a little counter that rips through the string it is given and increases the value of a variable upon each
#character occurence, which it returns in the end
def sort_proteins_aa(proteins,ALT):
	def countra(txt,ltr):
		cnt = 0
		for j in txt:
			if j == ltr:
				cnt = cnt+1
#use countra togo through the seq attribute (which is the 7th) and sort the proteins based on this. similar as above
		return cnt
	protaa = sorted(proteins, key = lambda x: countra(x[7],ALT), reverse = True)
	return protaa
#similar to the fasta_load, initializes an empty list then iterates through each input list elemnts, examines whether or not they have the supplied motif, if yes, appends
#the element to the new list which it returns in the end
def find_protein_with_motif(proteins,MOTIF):
	protaa = []
	for l in range(len(proteins)):
		if MOTIF in proteins[l].seq:
			protaa.append(proteins[l])
		else:
			pass
	return protaa

